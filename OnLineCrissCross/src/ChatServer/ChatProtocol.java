/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ChatServer;

import java.net.*;
import java.io.*;

/**
 *
 * @author balikm1
 */
public class ChatProtocol {
    
    private static final int WAITING = 0;
    private static final int USER = 1;
    private static final int BYE = 2;

    private int state = WAITING;

    public String processInput(String theInput) {
        String theOutput = null;

        if (state == WAITING) {
            theOutput = "Napis mi sem neco:";
            state = USER;
        } else if (state == USER) {
            theOutput = "Zprava byla dorucena! Rozluc se:";
            state = BYE;
        } else if (state == BYE) {
            theOutput = "Bye.";
            state = WAITING;
        }
        return theOutput;
    }
    
}