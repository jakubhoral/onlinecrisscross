/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swingcrisscross;

import javax.swing.JPanel;

/**
 *
 * @author Admin
 */
public class Game {

    Player one;
    Player two;
    boolean playerOneturn = true;

    public Game(Player one, Player two) {
        this.one = one;
        this.two = two;
    }

    

    public Player getPlayer() {
        if (playerOneturn) {
            return this.one;
        }
        return two;
    }

    public Player getOne() {
        return one;
    }
    
    

    public void switchPlayer() {
        playerOneturn = !playerOneturn;
    }

    public boolean checkWinner(JGameFieldButton board[][]) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j].toString().equals(this.getPlayer().toString())) {
                    int countr = 0;
                    int countc = 0;
                    int countdDown = 0;
                    int countdUp = 0;
                    int o = 0;
                    while (o < 5) {
//                            kontrola rady
                        if ((j + o) < board.length) {
                            if (board[i][j + o].toString().equals(this.getPlayer().toString())) {
                                countr++;
                            }
                        }
//                            kontrola sloupce
                        if ((i + o) < board.length) {
                            if (board[i + o][j].toString().equals(this.getPlayer().toString())) {
                                countc++;
                            }
                        }
//                            kontrola diagonalne dolu
                        if ((i + o) < board.length && (j + o) < board.length) {
                            if (board[i + o][j + o].toString().equals(this.getPlayer().toString())) {
                                countdDown++;
                            }
                        }
//                            kontrola diagonalne nahoru
                        if ((i + o) < board.length && (j - o) > 0) {

                            if (board[i + o][j - o].toString().equals(this.getPlayer().toString())) {
                                countdUp++;
                            }
                        }
                        o++;
                    }
                    if (countr > 4 || countc > 4 || countdUp > 4 || countdDown > 4) {
                        return true;
                    }

                }
            }
        }
        return false;
    }
}
