/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swingcrisscross;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Admin
 */
public class Window {

    JPanel windowContent;
    JPanel xCrossWin;
    JPanel menu;
    JGameFieldButton[][] buttons;
    JTextField textField;
    JButton start;
    JButton quit;
    Game game;

    public Window() {
        game = new Game(new Player("x"), new Player("o"));
        JFrame frame = new JFrame("Criss Cross");
        windowContent = new JPanel(new BorderLayout(2, 2));
        menu = new JPanel(new GridLayout(3, 1));
        start = new JButton("Start");
        quit = new JButton("Quit");
        textField = new JTextField(2);
        windowContent.add("East", menu);
        menu.add(textField, 0);
        menu.add(start, 1);
        menu.add(quit, 2);
        start.addActionListener((e) -> {
            try {
                int size = Integer.parseInt(textField.getText());
                if (size < 5 || size > 40) 
                  throw new Exception("Choose field size between 5-40");                  
                start.setVisible(false);
                buttons = new JGameFieldButton[size][size];
                xCrossWin = new JPanel(new GridLayout(size, size));
                int buttonId = 0;
                for (int i = 0; i < size; i++) {
                    for (int j = 0; j < size; j++) {
                        buttons[i][j] = new JGameFieldButton("");
                        xCrossWin.add(buttons[i][j], buttonId);
                        buttonId++;
                        buttons[i][j].setSize(2, 2);
                        buttons[i][j].addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                JGameFieldButton button = ((JGameFieldButton) e.getSource());
                                if (button.getText().equals("")) {
                                    if (true) {
                                        button.setText(game.getPlayer().toString());
                                        if (game.getPlayer().toString().equals(game.getOne().toString())) {
                                            button.setBackground(Color.red);
                                        }
                                        else{
                                            button.setBackground(Color.yellow);
                                        }
                                        if (game.checkWinner(buttons)) {
                                            JOptionPane.showMessageDialog(frame, "And the WINNER is:\nPlayer " + game.getPlayer());
                                            System.exit(0);
                                        }
                                        game.switchPlayer();
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(frame, "Field is already choosen by player: "+button.getText());                                           
                                }
                            }
                        });
                    }
                }
                windowContent.add("Center", xCrossWin);
                frame.setSize(size * 100, size * 100);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(frame, ex.getMessage());
            }
        });
        quit.addActionListener((e) -> {
            System.exit(0);
        });
        frame.setContentPane(windowContent);
        frame.pack();
        frame.setSize(600, 600);
        frame.setVisible(true);
    }
}
